# CarCar

Team:

* Davis Jkwon - Sale microservice
* John - Automobile service microservice

## Design

## Service microservice
Models include Technician, AutomobileVO, and Appointment. Poller will poll data from the inventory microservice to update the AutomobileVOs.

The Technician model will first_name(CharField), last_name(CharField), and employee_id(CharField) fields. The Appointment model will have date_time(DateTimeField), reason(TextField), status(CharField), vin(CharField), customer(CharField) and technician(foreignkey) fields. AutomobileVO model will contain vin(CharField) and sold(Bool) fields.

## Sales microservice

Models include AutomobileVO, Salesperson, Customer and Sale. The poller will pull from the inventory mircroservice to allow changes
to the VO within sale's microservice.
