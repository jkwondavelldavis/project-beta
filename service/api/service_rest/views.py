from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from django.db import IntegrityError, DataError

from .models import AutomobileVO, Technician, Appointment
from .encoders import AppointmentEncoder, TechnicianEncoder

@require_http_methods(["GET","POST"])
def api_technicians(request):
    if request.method == "GET":
        return JsonResponse(
            {"technicians": Technician.objects.all()},
            encoder=TechnicianEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            return JsonResponse(
                Technician.objects.create(**content),
                encoder=TechnicianEncoder,
                safe=False
            )
        except IntegrityError:
            return JsonResponse(
                {"message":"Employee id already assigned to another employee."},
                status=400
            )

@require_http_methods("DELETE")
def api_technician(request, id):
    if request.method == "DELETE":
        count, _ = Technician.objects.filter(id=id).delete()
        if count > 0:
                return JsonResponse({"Deleted":count>0})
        else:
            return JsonResponse(
                {"message":f"Technician with id: {id} does not exist."},
                status = 400
            )

@require_http_methods(["GET","POST"])
def api_appointments(request):
    if request.method == "GET":
        return JsonResponse(
            {"appointments":Appointment.objects.all().order_by('id')},
            encoder=AppointmentEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            content["technician"]=Technician.objects.get(employee_id=content["technician"])
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message":"Invalid employee_id"},
                status=400
            )
        content["vip"] = AutomobileVO.objects.filter(vin=content["vin"]).count() > 0
        try:
            appointment = Appointment.objects.create(**content)
        except DataError:
            return JsonResponse(
                {"message":"Data Error. Check length of inputs."},
                status = 400
            )
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )

@require_http_methods(["PUT", "DELETE"])
def api_appointment(request, id, status=None):
    if request.method == "PUT":
        content = {}
        if status == "finish" or status == "cancel":
            content["status"]=f'{status}ed'
        else:
            return JsonResponse(
                {"message":"Status must be 'finish' or 'cancel'."},
                status = 400
            )
        try:
            Appointment.objects.filter(id=id).update(**content)
            return JsonResponse(
                Appointment.objects.get(id=id),
                encoder=AppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message":f"Appointment of id:{id} does not exist."},
                status = 400
            )
    else:
        count, _ = Appointment.objects.filter(id=id).delete()
        if count > 0:
            return JsonResponse({"Deleted":count>0})
        else:
            return JsonResponse(
                {"message":f"Appointment of id:{id} does not exist."},
                status = 400
            )
