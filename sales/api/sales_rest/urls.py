from django.urls import path
from .views import (
    api_list_salespeople,
    api_show_saleperson,
    api_list_customers,
    api_show_customer,
    api_list_sales,
    api_show_sale)

urlpatterns = [
    path("salespeople/", api_list_salespeople, name="api_list_salespeople"),
    path("salespeople/<int:pk>/", api_show_saleperson, name="api_show_saleperson"),
    path("customers/", api_list_customers, name="api_list_customers"),
    path("customers/<int:pk>/", api_show_customer, name="api_show_customer"),
    path("sales/", api_list_sales, name="api_list_sales"),
    path("sales/<int:pk>/", api_show_sale, name="api_show_sale")
]
