from django.shortcuts import render
from .models import Salesperson, Sale, Customer, AutomobileVO
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from common.json import ModelEncoder
import requests


# List Encoder for automobile
class AutomobileVOListEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_vin",
        "sold"
    ]


# List Encoder for salespeople
class SalespeopleListEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


# List Encoder for customer
class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number"
    ]


# List Encoder for sales
class SaleListEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "automobile",
        "saleperson",
        "customer",
        "price"
    ]
    encoders = {
        "automobile": AutomobileVOListEncoder(),
        "saleperson": SalespeopleListEncoder(),
        "customer": CustomerListEncoder(),
    }


# Create your views here.


# Create a list view for salespeople
@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespeopleListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salespeople = Salesperson.objects.create(**content)
            return JsonResponse(
                salespeople,
                encoder=SalespeopleListEncoder,
                safe=False,
            )
        except Exception:
            response = JsonResponse(
                {"message": "Could not create salesperson"}
            )
            response.status_code = 400
            return response


# Cretae a detail view for a saleperson
@require_http_methods(["DELETE", "GET"])
def api_show_saleperson(request, pk):
    if request.method == "GET":
        saleperson = Salesperson.objects.get(id=pk)
        return JsonResponse(
            saleperson,
            encoder=SalespeopleListEncoder,
            safe=False
        )

    else:
        try:
            saleperson = Salesperson.objects.get(id=pk)
            saleperson.delete()
            return JsonResponse(
                {"saleperson": "deleted"}
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Salesperson does not exist"})
            response.status_code = 400
            return response


# Create a list view for customers
@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerListEncoder,
                safe=False,
            )
        except Exception:
            response = JsonResponse(
                {"message": "Could not create the salesperson"}
            )
            response.status_code = 400
            return response


# Create a detail view for customer
@require_http_methods(["DELETE", "GET"])
def api_show_customer(request, pk):
    if request.method == "GET":
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerListEncoder,
            safe=False
        )
    else:
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                {"customer": "deleted"})

        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist"})
            response.status_code = 400
            return response


# Create a list view for sale
@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleListEncoder,
            safe=False,
            )
    else:
        content = json.loads(request.body)
        salecontent = {}
        vin = content["vin"]
        automobile = AutomobileVO.objects.get(import_vin=vin)
        salecontent["automobile"] = automobile
        employee_id = content["employee_id"]
        salesperson = Salesperson.objects.get(employee_id=employee_id)
        salecontent["saleperson"] = salesperson
        price = content["price"]
        salecontent["price"] = price
        phone_number = content["phone_number"]
        customer = Customer.objects.get(phone_number=phone_number)
        salecontent["customer"] = customer
        sale = Sale.objects.create(**salecontent)
        sold = {
            "sold": True
        }
        requests.put(f"http://project-beta-inventory-api-1:8000/api/automobiles/{vin}/", json=sold)
        return JsonResponse(
            sale,
            encoder=SaleListEncoder,
            safe=False
        )


@require_http_methods(["DELETE"])
def api_show_sale(request, pk):
    try:
        sale = Sale.objects.get(id=pk)
        sale.delete()
        return JsonResponse(
                {"sale": "deleted"}
            )
    except Sale.DoesNotExist:
        response = JsonResponse({"message": "Sale does not exist"})
        response.status_code = 400
        return response
