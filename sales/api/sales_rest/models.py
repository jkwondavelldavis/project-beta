from django.db import models


class AutomobileVO(models.Model):
    import_vin = models.CharField(max_length=17, unique=True)
    sold = models.CharField(max_length=50)

    def __str__(self):
        return self.import_vin


class Salesperson(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=8)

    def __str__(self):
        return self.first_name + " " + self.last_name


class Customer(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=10)

    def __str__(self):
        return self.first_name + " " + self.last_name


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobiles",
        on_delete=models.CASCADE,
    )

    saleperson = models.ForeignKey(
        Salesperson,
        related_name="salespesons",
        on_delete=models.CASCADE,
    )

    customer = models.ForeignKey(
        Customer,
        related_name="customers",
        on_delete=models.CASCADE,
    )

    price = models.CharField(max_length=50)
