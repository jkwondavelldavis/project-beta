import React, {useEffect, useState} from "react";

export default function AutomobileList(){
    const [autos, setAutos] = useState([])

    async function fetchAutos(){
        const response = await fetch("http://localhost:8100/api/automobiles/")

        if (response.ok){
            const data = await response.json();
            setAutos(data.autos);
        }
    }

    useEffect(()=>{
        fetchAutos();
    },[])

    return(
        <div>
            <h1>Automobiles</h1>
            <table className="table table-hover table-striped ">
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>Make</th>
                        <th>Model</th>
                        <th>Year</th>
                        <th>Color</th>
                        <th>Sold?</th>
                    </tr>
                </thead>
                <tbody>
                    {autos.map(auto => {
                        return (
                            <tr key={auto.vin}>
                                <td>{auto.vin}</td>
                                <td>{auto.model.manufacturer.name}</td>
                                <td>{auto.model.name}</td>
                                <td>{auto.year}</td>
                                <td>{auto.color}</td>
                                <td>{`${auto.sold}`}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
