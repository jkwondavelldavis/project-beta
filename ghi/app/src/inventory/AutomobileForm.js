import { useState, useEffect  } from "react"

export default function AutomobileForm(){
    const [formData, setFormData] = useState({
        color: "",
        year: "",
        vin: "",
        model_id: ""
    })

    const handleChange=(event) =>{
        const{name,value} = event.target
        setFormData((prevFormData)=>({...prevFormData, [name]: value}))
    }

    const handleSubmit = async (event) =>{
        event.preventDefault()
        const url = "http://localhost:8100/api/automobiles/"
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        fetch(url, fetchOptions)
        window.location.reload()
    }

    const [models, setModels] = useState([])
    async function fetchModels(){
        const response = await fetch("http://localhost:8100/api/models/")

        if (response.ok){
            const data = await response.json();
            setModels(data.models);
        }
    }

    useEffect(()=>{
        fetchModels();
    },[])

    return (
        <div className="row offset-3 col-6 shadow p-4 mt-4">
            <h1>Add a Vehicle</h1>
            <form onSubmit={handleSubmit} id="create-automobile-form">
                <div className="form-floating mb-3">
                    <input onChange={handleChange} id="color" name="color" className="form-control" required type="text"
                        value={formData.color}/>
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleChange} id="year" name="year" className="form-control" required type="text"
                        value={formData.year}/>
                    <label htmlFor="year">Year</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleChange} id="vin" name="vin" className="form-control" required type="text"
                        value={formData.vin}/>
                    <label htmlFor="vin">Vin</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleChange} id="model_id" name="model_id" className="form-select" value={formData.technician}>
                        <option defaultValue value="">Choose a model</option>
                        {models.sort((a,b)=>{
                            return a.manufacturer.name < b.manufacturer.name ? -1 :
                            a.manufacturer.name > b.manufacturer.name ? 1 : 0
                        }).map(model =>{
                            return(
                                <option key={model.id} value={model.id}>{model.manufacturer.name}-{model.name}</option>
                            )
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
    )


}
