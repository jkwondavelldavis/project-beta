import React, {useEffect, useState} from "react";


export default function ManufacturerList(){
    const [manufacturers, setManufacturer] = useState([])

    async function fetchManufacturers(){
        const response = await fetch("http://localhost:8100/api/manufacturers/")

        if (response.ok){
            const data = await response.json();
            setManufacturer(data.manufacturers);
        }
    }

    useEffect(()=>{
        fetchManufacturers();
    }, [])

    return(
        <div>
            <h1>Manufacturers</h1>
            <table className="table table-hover table-striped ">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manufacturer => {
                        return (
                            <tr key={manufacturer.id}>
                                <td>{manufacturer.name}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
