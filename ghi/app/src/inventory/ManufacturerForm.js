import { useState, useEffect  } from "react"


 export default function ManufacturerForm(){
    const [formData, setFormData] = useState({
        name: "",
    })

    const handleChange = (event) =>{
        const {name, value} = event.target
        setFormData((prevFormData)=>({...prevFormData, [name] :value}))
    }


    const handleSubmit = async (event) =>{
        event.preventDefault()
        const url = "http://localhost:8100/api/manufacturers/"
        const fetchOptions ={
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        fetch(url, fetchOptions)
    }

    return (
        <div className="row offset-3 col-6 shadow p-4 mt-4">
            <h1>Create a manufacturer</h1>
            <form onSubmit={handleSubmit} id="create-technician-form">
                <div className="form-floating mb-3">
                    <input onChange={handleChange} id="name" name="name" className="form-control" required type="text"
                        value={formData.name}/>
                    <label htmlFor="name">Manufacturers name..</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
    )
}
