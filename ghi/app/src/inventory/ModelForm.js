import { useState, useEffect  } from "react"


export default function ModelForm() {
    const [formData, setFormData] = useState({
        name: "",
        picture_url: "",
    })

    const handleChange = (event) =>{
        const {name, value} = event.target
        setFormData((prevFormData)=>({...prevFormData, [name] :value}))
    }


    const handleSubmit = async (event) =>{
        event.preventDefault()
        const url = "http://localhost:8100/api/models/"
        const fetchOptions ={
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        fetch(url, fetchOptions)
        window.location.reload();
    }


    const [manufacturer_id, setManufacturerId] = useState([])
    const fetchManufacturers = async () => {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            setManufacturerId(data.manufacturers)
        }
        else {
            throw new Error("Response not ok")
        }
    }
    useEffect(() => {
        fetchManufacturers();
    }, []);


    return (
        <div className="row offset-3 col-6 shadow p-4 mt-4">
            <h1>Create a vehicle model</h1>
            <form onSubmit={handleSubmit} id="create-technician-form">
                <div className="form-floating mb-3">
                    <input onChange={handleChange} id="name" name="name" className="form-control" required type="text"
                        value={formData.name}/>
                    <label htmlFor="name">Model name..</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleChange} id="picture_url" name="picture_url" className="form-control" required type="text"
                        value={formData.picture_url}/>
                    <label htmlFor="name">Picture URL..</label>
                </div>
                <div className="form-floating mb-3">
                    <select onChange={handleChange} id="manufacturer_id" name="manufacturer_id" className="form-select">
                        <option>Choose a manufacturer</option>
                        {manufacturer_id.map(manufacturer => {
                            return (
                                <option key={manufacturer.id} value={manufacturer.id}>{ manufacturer.name}</option>
                            )
                        })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
    )
}
