import React, {useEffect, useState} from "react";

export default function ModelList(){
    const [models, setModels] = useState([])

    async function fetchModels(){
        const response = await fetch("http://localhost:8100/api/models/")

        if (response.ok){
            const data = await response.json();
            setModels(data.models);
        }
    }

    useEffect(()=>{
        fetchModels();
    },[])

    return(
        <div>
            <h1>Models</h1>
            <table className="table table-hover table-striped ">
                <thead>
                    <tr>
                        <th>Model</th>
                        <th>Make</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return (
                            <tr key={model.id}>
                                <td>{model.name}</td>
                                <td>{model.manufacturer.name}</td>
                                <td>
                                    <img src={model.picture_url} alt={model.name}/>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
