import { useState, useEffect  } from "react"

function AppointmentForm(){
    const [formData, setFormData] = useState({
        date_time: new Date().toISOString().slice(0,16),
        reason: "",
        customer: "",
        vin: "",
        technician: ""
    })

    const handleChange = (event) =>{
        const {name, value} = event.target
        setFormData((prevFormData)=>({...prevFormData, [name] :value}))
    }


    const handleSubmit = async (event) =>{
        event.preventDefault()
        const url = "http://localhost:8080/api/appointments/"
        const fetchOptions ={
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        fetch(url, fetchOptions)
    }

    const [technicians, setTechnicians] = useState([])
    async function fetchTechs(){
        const response = await fetch("http://localhost:8080/api/technicians/")

        if (response.ok){
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    }

    useEffect(()=>{
        fetchTechs();
    }, [])

    return (
        <div className="row offset-3 col-6 shadow p-4 mt-4">
            <h1>Create an appointment</h1>
            <form onSubmit={handleSubmit} id="create-appointment-form">
                <div className="form-floating mb-3">
                    <input onChange={handleChange} id="customer" name="customer" className="form-control" required type="text"
                        value={formData.customer}/>
                    <label htmlFor="customer">Customer name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleChange} id="vin" name="vin" className="form-control" required type="text"
                        value={formData.vin}/>
                    <label htmlFor="vin">VIN</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleChange} id="reason" name="reason" className="form-control" required type="text"
                        value={formData.reason}/>
                    <label htmlFor="reason">Reason</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleChange} id="date_time" name="date_time" className="form-control" required type="datetime-local"
                        />
                    <label htmlFor="date_time">Date & time</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleChange} id="technician" name="technician" className="form-select" value={formData.technician}>
                        <option defaultValue value="">Choose a technician</option>
                        {technicians.map(tech =>{
                            return(
                                <option key={tech.employee_id} value={tech.employee_id}>{tech.last_name} {tech.employee_id}</option>
                            )
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
    )
}


export default AppointmentForm
