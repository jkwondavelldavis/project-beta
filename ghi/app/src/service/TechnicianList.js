import React, {useEffect, useState} from "react";
import {redirect} from "react-router-dom";


function TechnicianList(){
    const [technicians, setTechnicians] = useState([])

    async function fetchTechs(){
        const response = await fetch("http://localhost:8080/api/technicians/")

        if (response.ok){
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    }

    useEffect(()=>{
        fetchTechs();
    }, [])

    return(
        <div className="m-5">
            <h1>Technicians</h1>
            <table className="table table-hover table-striped ">
                <thead>
                    <tr>
                        <th>Last name</th>
                        <th>First name</th>
                        <th>Employee id</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians.map(tech => {
                        return (
                            <tr key={tech.employee_id}>
                                <td>{tech.last_name}</td>
                                <td>{tech.first_name}</td>
                                <td>{tech.employee_id}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}


export default TechnicianList
