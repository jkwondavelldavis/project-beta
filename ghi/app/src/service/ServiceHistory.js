import { useState, useEffect } from "react"
import { FormatDate } from "./AppointmentList"

export default function ServiceHistory(){
    const [searchField, setSearchField] = useState("")
    const [search, setSearch] = useState("")

    const handleSearch = (event) =>{
        setSearchField(event.target.value)
    }
    const submitSearch = (event) =>{
        event.preventDefault()
        setSearch(searchField)
    }

    return(
        <div className="m-5">
            <h1>Service History</h1>
            <div className="form-floating">
                <form onSubmit={submitSearch} id="search-form">
                    <div className="input-group" style={{width:'20em'}}>
                        <input onChange={handleSearch} id="search" name="search" className="form-control" placeholder="Search vin" />
                        <div className="input-group-append">
                            <button className="btn btn-secondary">Search</button>
                        </div>
                    </div>
                </form>
            </div>
            <SearchTable search={search}/>
        </div>
    )
}

function SearchTable({search}){
    const [appointments, setAppointments] = useState([])
    async function fetchAppointments(){
        const response = await fetch("http://localhost:8080/api/appointments/")

        if (response.ok){
            const data = await response.json();
            setAppointments(data.appointments);
        }
    }

    useEffect(()=>{
        fetchAppointments();
    }, [])

    return(
        <table className="table table-hover table-striped ">
            <thead>
                <tr>
                    <th>VIP</th>
                    <th>Technician</th>
                    <th>Time</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Reason</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                {appointments.filter(appt => appt.vin.startsWith(search) ).map(appt => {
                    return (
                        <tr key={appt.href}>
                            <td>{`${appt.vip}`}</td>
                            <td>{appt.technician.last_name}</td>
                            <td>{FormatDate(appt.date_time)}</td>
                            <td>{appt.customer}</td>
                            <td>{appt.vin}</td>
                            <td>{appt.reason}</td>
                            <td>
                                {appt.status}
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}
