import React, {useEffect, useState} from "react";


async function updateAppointment(href) {
  const response = await fetch("http://localhost:8080" + href, { method: "PUT" },)
  window.location.reload()
}

export function FormatDate(date_time_string){
    const date_time = new Date(date_time_string)
    const locale = 'en-US'
    return (`${date_time.toLocaleDateString(locale,{weekday:'short',month:'short', day:'2-digit',year:'numeric'})}
        ${date_time.toLocaleTimeString(locale,{hour:'2-digit',minute:'2-digit',hour12:true})}`)
}

export default function AppointmentList(){
    const [appointments, setAppointments] = useState([])

    async function fetchAppointments(){
        const response = await fetch("http://localhost:8080/api/appointments/")

        if (response.ok){
            const data = await response.json();
            setAppointments(data.appointments);
        }
    }

    useEffect(()=>{
        fetchAppointments();
    }, [])

    return(
        <div className="m-5">
            <h1>Appointments</h1>
            <table className="table table-hover table-striped ">
                <thead>
                    <tr>
                        <th>VIP</th>
                        <th>Technician</th>
                        <th>Time</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Reason</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.filter(appt => appt.status === "pending").map(appt => {
                        return (
                            <tr key={appt.href}>
                                <td>{`${appt.vip}`}</td>
                                <td>{appt.technician.last_name}</td>
                                <td>{FormatDate(appt.date_time)}</td>
                                <td>{appt.customer}</td>
                                <td>{appt.vin}</td>
                                <td>{appt.reason}</td>
                                <td>
                                    {appt.status}
                                </td>
                                <td>
                                    <button className="btn btn-success" onClick={() => { updateAppointment(appt.href +"finish") }}>
                                        Finish
                                    </button>
                                    <button className="btn btn-danger" onClick={() => { updateAppointment(appt.href +"cancel") }}>
                                        Cancel
                                    </button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>


    )
}
