import { useState, useEffect  } from "react"


function TechnicianForm(){
    const [formData, setFormData] = useState({
        first_name: "",
        last_name: "",
        employee_id: ""
    })

    const handleChange = (event) =>{
        const {name, value} = event.target
        setFormData((prevFormData)=>({...prevFormData, [name] :value}))
    }


    const handleSubmit = async (event) =>{
        event.preventDefault()
        const url = "http://localhost:8080/api/technicians/"
        const fetchOptions ={
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        fetch(url, fetchOptions)
    }

    return (
        <div className="row offset-3 col-6 shadow p-4 mt-4">
            <h1>Add technician</h1>
            <form onSubmit={handleSubmit} id="create-technician-form">
                <div className="form-floating mb-3">
                    <input onChange={handleChange} id="first_name" name="first_name" className="form-control" required type="text"
                        value={formData.first_name}/>
                    <label htmlFor="first_name">First name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleChange} id="last_name" name="last_name" className="form-control" required type="text"
                        value={formData.last_name}/>
                    <label htmlFor="last_name">Last name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleChange} id="employee_id" name="employee_id" className="form-control" required type="text"
                        value={formData.employee_id}/>
                    <label htmlFor="employee_id">Employee ID</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
    )
}


export default TechnicianForm
