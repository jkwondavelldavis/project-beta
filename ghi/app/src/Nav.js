import { NavLink, useMatch, useResolvedPath } from 'react-router-dom';

function CustomNav({to,children, ...props}){
  const resolvedPath = useResolvedPath(to)
  const match = useMatch({path: resolvedPath.pathname.split('/')[1], end: false})
  if(!match){
    props["className"]+= ' d-none'
  }
  else{
    props["className"]= props["className"].split("d-none").join('')
  }
  return(<NavLink to={to} {...props}>{children}</NavLink>)
}

function Nav() {
  return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
          <NavLink className="navbar-brand" to="/">CarCar</NavLink>
          <CustomNav className="navbar-brand" to="sales/salespeople">Salespeople</CustomNav>
          <CustomNav className="navbar-brand" to="sales/salespeople/create">Add Salesperson</CustomNav>
          <CustomNav className="navbar-brand" to="sales/customers">Customers</CustomNav>
          <CustomNav className="navbar-brand" to="sales/customers/create">Add a Customer</CustomNav>
          <NavLink className="navbar-brand" to="sales">Sales</NavLink>
          <CustomNav className="navbar-brand" to="sales/create">Add a Sale</CustomNav>
          <CustomNav className="navbar-brand" to="sales/history">Salesperson History</CustomNav>

          <NavLink className="navbar-brand" to="service">Service</NavLink>
          <CustomNav className="navbar-brand" to="service/technicians">Technicians</CustomNav>
          <CustomNav className="navbar-brand" to="service/technicians/add">Add Technician</CustomNav>
          <CustomNav className="navbar-brand" to="service/appointments">Appointments</CustomNav>
          <CustomNav className="navbar-brand" to="service/appointments/add">Create Appointment</CustomNav>
          <CustomNav className="navbar-brand" to="service/appointments/history">Service History</CustomNav>

          <NavLink className="navbar-brand" to="inventory">Inventory</NavLink>
          <CustomNav className="navbar-brand" to="inventory/manufacturers">Manufacturers</CustomNav>
          <CustomNav className="navbar-brand" to="inventory/manufacturers/add">Add Manufacturer</CustomNav>
          <CustomNav className="navbar-brand" to="inventory/models">Models</CustomNav>
          <CustomNav className="navbar-brand" to="inventory/models/add">Add Model</CustomNav>
          <CustomNav className="navbar-brand" to="inventory/automobiles">Automobiles</CustomNav>
          <CustomNav className="navbar-brand" to="inventory/automobiles/add">Add Automobile</CustomNav>

          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            </ul>
          </div>
        </div>
      </nav>
  )
}

export default Nav;
