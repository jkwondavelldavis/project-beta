import React, { useEffect, useState } from "react";

export default function Customers() {
    const [customers, setCustomers] = useState([]);

    async function fetchCustomers() {
        const response = await fetch("  http://localhost:8090/api/customers/")
        if (response.ok) {
            const data = await response.json()
            setCustomers(data.customers)
        }
    }
    useEffect(() => {
        fetchCustomers();
    }, []);

    return (
        <div className="div m-5">
            <h1>Customers</h1>
            <table className="table table-hover table-striped ">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Phone Number</th>
                        <th>Address</th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map(customer => {
                        return (
                            <tr key={customer.id}>
                                <td>{customer.first_name}</td>
                                <td>{customer.last_name}</td>
                                <td>{customer.phone_number.replace(/(\d{3})(\d{3})(\d{4})/, "($1)-$2-$3")}</td>
                                <td>{customer.address}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
