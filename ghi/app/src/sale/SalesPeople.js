import React, { useEffect, useState } from "react";

export default function SalesPeople() {
    const [sales, setSales] = useState([]);

    async function fetchSalesPeople() {
        const response = await fetch("	http://localhost:8090/api/salespeople/")
        if (response.ok) {
            const data = await response.json()
            setSales(data.salespeople)
        }
    }
    useEffect(() => {
        fetchSalesPeople();
    }, []);


    return (
        <div className="div m-5">
            <h1>Salespeople</h1>
            <table className="table table-hover table-striped ">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.employee_id}</td>
                                <td>{sale.first_name}</td>
                                <td>{sale.last_name}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
