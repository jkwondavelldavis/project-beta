import React, { useEffect, useState } from "react";

export default function Sale() {
    const [sales, setSales] = useState([]);

    async function fetchsales() {
        const response = await fetch("	http://localhost:8090/api/sales/")
        if (response.ok) {
            const data = await response.json()
            setSales(data.sales)
        }
    }
    useEffect(() => {
        fetchsales();
    }, []);


    return (
        <div className="div m-5 ">
            <h1>Sales</h1>
            <table className="table table-hover table-striped ">
                <thead>
                    <tr>
                        <th>Saleperson Employee ID</th>
                        <th>Saleperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.saleperson.employee_id}</td>
                                <td>{sale.saleperson.first_name + " " + sale.saleperson.last_name}</td>
                                <td>{sale.customer.first_name + " " + sale.customer.last_name}</td>
                                <td>{sale.automobile.import_vin}</td>
                                <td>${sale.price}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
