import React, { useEffect, useState } from "react";


export default function CustomerForm() {
  const [first_name, setFirstName] = useState('');
  const [last_name, setLastName] = useState("");
  const [address, setAddress] = useState('');
  const [phone_number, setPhoneNumber] = useState('');

  const fetchdata = async () => {
    const url = "http://localhost:8090/api/customers/";
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json();
    }
    else {
      throw new Error("Response not ok")
    }
  }
  useEffect(() => {
    fetchdata();
  }, []);

  const handleChangeFirstName = (event) => {
    const value = event.target.value;
    setFirstName(value);
  }

  const handleChangeLastName = (event) => {
    const value = event.target.value;
    setLastName(value);
  }

  const handleChangeAddress = (event) => {
    const value = event.target.value;
    setAddress(value);
  }
  const handleChangePhoneNumber= (event) => {
    const value = event.target.value;
    setPhoneNumber(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.first_name = first_name;
    data.last_name = last_name;
    data.address = address;
    data.phone_number = phone_number;

    const url = " http://localhost:8090/api/customers/"
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      },
    };

    const response = await fetch(url, fetchOptions);
    if (response.ok) {
      await response.json();

      setFirstName('');
      setLastName('');
      setAddress('');
      setPhoneNumber('');

    } else {
      console.error("Bad request")
    }
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Customer</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input onChange={handleChangeFirstName}  value={first_name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">First name..</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeLastName} value={last_name} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
              <label htmlFor="last_name">Last name..</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeAddress} value={address} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
              <label htmlFor="address">Address..</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangePhoneNumber} value={phone_number} placeholder="City" required type="text" name="phone_number" id="phone_number" className="form-control" />
              <label htmlFor="phone_number">Phone Number..</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}
