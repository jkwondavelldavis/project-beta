import React, { useEffect, useState } from "react";

export default function SalePersonHistory() {
    const [sales, setSales] = useState([]);
    const [salespeople, setSalesPeople] = useState([]);
    const [salesperson, setSalesPerson] = useState("");

    async function fetchsales() {
        const response = await fetch("	http://localhost:8090/api/sales/")
        if (response.ok) {
            const data = await response.json()
            setSales(data.sales)
        }
    }
    useEffect(() => {
        fetchsales();
    }, []);


    async function fetchSalesPeople() {
        const response = await fetch("	http://localhost:8090/api/salespeople/")
        if (response.ok) {
            const data = await response.json()
            setSalesPeople(data.salespeople)
        }
    }
    useEffect(() => {
        fetchSalesPeople();
    }, []);

    const handleOptionChange = (e) => {
        const value = e.target.value;
        setSalesPerson(value)
    }


    return (
        <div>
            <h1>Salesperson History</h1>
            <div className="row">
                <form>
                    <div className="mb-3">
                        <select onChange={handleOptionChange} value={salesperson} className="form-select">
                            <option>Choose a salesperson</option>
                            {salespeople.map(salepeople => {
                                return (
                                    <option key={salepeople.id} value={salepeople.first_name + " " + salepeople.last_name}>{salepeople.first_name + " " + salepeople.last_name}</option>
                                )
                            })}

                        </select>
                    </div>
                </form>

            </div>
            <table className="table table-hover table-striped ">
                <thead>
                    <tr>
                        <th>Saleperson Employee ID</th>
                        <th>Saleperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.filter(sale => sale.saleperson.first_name + " " + sale.saleperson.last_name === salesperson).map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.saleperson.employee_id}</td>
                                <td>{sale.saleperson.first_name + " " + sale.saleperson.last_name}</td>
                                <td>{sale.customer.first_name + " " + sale.customer.last_name}</td>
                                <td>{sale.automobile.import_vin}</td>
                                <td>${sale.price}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
