import React, { useEffect, useState } from "react";


export default function RecordForm() {
    const [automobiles, setAutomobiles] = useState([]);
    const [salespepoles, setSalespeoples] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [vin, setVin] = useState('');
    const [employee_id, setEmployeeId] = useState('');
    const [phone_number, setPhoneNumber] = useState('');
    const [price, setPrice] = useState('');

    const fetchdata = async () => {
        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos)
        }
        else {
            throw new Error("Response not ok")
        }
    }
    useEffect(() => {
        fetchdata();
    }, []);

    const fetchsalespeople = async () => {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            setSalespeoples(data.salespeople)
        }
        else {
            throw new Error("Response not ok")
        }
    }
    useEffect(() => {
        fetchsalespeople();
    }, []);

    const fetchcustomers = async () => {
        const url = "http://localhost:8090/api/customers/";
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers)
        }
        else {
            throw new Error("Response not ok")
        }
    }
    useEffect(() => {
        fetchcustomers();
    }, []);

    const handleChangeVin = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleChangeEmployeeId = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }

    const handleChangePhoneNumber = (event) => {
        const value = event.target.value;
        setPhoneNumber(value);
    }

    const handleChangePrice = (event) => {
        const value = event.target.value;
        setPrice(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.vin = vin;
        data.employee_id = employee_id;
        data.phone_number = phone_number;
        data.price = price;


        const hatUrl = "http://localhost:8090/api/sales/";
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatUrl, fetchOptions);
        if (response.ok) {
            await response.json();


            setVin('');
            setPhoneNumber('');
            setEmployeeId('');
            setPrice('');


        }

    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a new sale</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                        <div className="mb-3">
                            <label htmlFor="automobile">Automobile VIN</label>
                            <select onChange={handleChangeVin} value={vin} className="form-select">
                                <option value="">Choose a automobile vin</option>
                                {automobiles.filter(automobile => automobile["sold"] == false).map(automobile => {
                                    return (
                                        <option key={automobile.id} value={automobile.vin}>{automobile.vin}</option>
                                    )
                                })}

                            </select>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="saleperson">Saleperson</label>
                            <select onChange={handleChangeEmployeeId} value={employee_id} className="form-select">
                                <option value="">Choose a salesperson</option>
                                {salespepoles.map(salepeople => {
                                    return (
                                        <option key={salepeople.id} value={salepeople.employee_id}>{salepeople.first_name + " " + salepeople.last_name}</option>
                                    )
                                })}

                            </select>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="customer">Customer</label>
                            <select onChange={handleChangePhoneNumber} value={phone_number} className="form-select" id="customer">
                                <option value="">Choose a customer</option>
                                {customers.map(customer => {
                                    return (
                                        <option key={customer.id} value={customer.phone_number}>{customer.first_name + " " + customer.last_name}</option>
                                    )
                                })}

                            </select>
                        </div>
                        <div className="form-group">
                            <label htmlFor="price">Price</label>
                            <input onChange={handleChangePrice} value={price} type="number" className="form-control" id="exampleInputPassword1" placeholder="0" />
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}