import React, { useEffect, useState } from "react";


export default function SalePersonForm() {
  const [firstname, setFirstName] = useState("");
  const [lastname, setLastName] = useState("");
  const [employeeid, setEmployeeId] = useState("");

  const fetchdata = async () => {
    const url = "	http://localhost:8090/api/salespeople/";
    const response = await fetch(url)
    if (response.ok) {
      const data = await response.json();
    }
    else {
      throw new Error("Response not ok")
    }
  }
  useEffect(() => {
    fetchdata();
  }, []);

  const handleChangeFirstName = (event) => {
    const value = event.target.value;
    setFirstName(value);
  }

  const handleChangeLastName = (event) => {
    const value = event.target.value;
    setLastName(value);
  }

  const handleChangeEmployeeId = (event) => {
    const value = event.target.value;
    setEmployeeId(value);
  }


  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.first_name = firstname;
    data.last_name = lastname;
    data.employee_id = employeeid;

    const url = "	http://localhost:8090/api/salespeople/"
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      },
    };

    const response = await fetch(url, fetchOptions);
    if (response.ok) {
      const newSalesperson = await response.json();


      setFirstName('');
      setLastName('');
      setEmployeeId('');
    } else {
      console.error("Bad request")
    }

  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Salesperson</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input onChange={handleChangeFirstName} value={firstname} placeholder="First_name" required type="text" name="first_name" id="first_name" className="form-control" />
              <label htmlFor="first_name">First name..</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeLastName} value={lastname} placeholder="last_name" required type="text" name="last_name" id="last_name" className="form-control" />
              <label htmlFor="last_name">Last name..</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeEmployeeId} value={employeeid} placeholder="employee_id" required type="text" name="employee_id" id="employee_id" className="form-control" />
              <label htmlFor="employee_id">Employee ID..</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}
