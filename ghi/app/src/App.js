import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';

import SalePersonForm from './sale/SalePersonForm';
import SalesPeople from './sale/SalesPeople';
import CustomerForm from './sale/CustomerForm';
import Customers from './sale/Customer';
import RecordForm from './sale/RecordForm';
import Sale from './sale/Sale';
import SalePersonHistory from './sale/SalesPersonHistory';

import AppoinmentList from './service/AppointmentList'
import AppointmentForm from './service/AppointmentForm'
import TechnicianList from './service/TechnicianList'
import TechnicianForm from './service/TechnicianForm'
import ServiceHistory from './service/ServiceHistory'

import AutomobileForm from "./inventory/AutomobileForm"
import AutomobileList from "./inventory/AutomobileList"
import ModelList from "./inventory/ModelList"
import ModelForm from "./inventory/ModelForm"
import ManufacturerForm from "./inventory/ManufacturerForm"
import ManufacturerList from "./inventory/ManufacturerList"

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container-fluid">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="sales">
            <Route path="" element={<Sale />}/>
            <Route path="create" element={<RecordForm />}/>
            <Route path="history" element={<SalePersonHistory />}/>
            <Route path="customers">
              <Route path="" element={<Customers />}></Route>
              <Route path="create" element={<CustomerForm />}></Route>
            </Route>
            <Route path="salespeople">
              <Route path="create" element={<SalePersonForm />} />
              <Route path="" element={<SalesPeople />} />
            </Route>
          </Route>

          <Route path="service">
            <Route path="" element={<MainPage />} />
            <Route path="technicians">
              <Route path="" element={<TechnicianList/>} />
              <Route path="add" element={<TechnicianForm/>}/>
            </Route>
            <Route path="appointments">
              <Route path="" element={<AppoinmentList/>} />
              <Route path="add" element={<AppointmentForm/>}/>
              <Route path="history" element={<ServiceHistory/>}/>
            </Route>
          </Route>

          <Route path="inventory">
            <Route path="" element={<MainPage/>}/>
            <Route path="automobiles">
              <Route path="" element={<AutomobileList/>}/>
              <Route path="add" element={<AutomobileForm/>}/>
            </Route>
            <Route path="models">
              <Route path="" element={<ModelList/>}/>
              <Route path="add" element={<ModelForm/>}/>
            </Route>
            <Route path="manufacturers">
              <Route path="" element={<ManufacturerList/>}/>
              <Route path="add" element={<ManufacturerForm/>}/>
            </Route>
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
